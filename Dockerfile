#!/bin/bash

# Define base docker image
FROM openjdk:8-jdk-alpine
# Install necessary packages to create a non-root user
RUN apk add --no-cache shadow && \
    groupadd -g 1001 myuser && \
    useradd -u 1001 -g myuser myuser && \
    mkdir /home/myuser && \
    chown myuser:myuser /home/myuser

# Set the working directory and copy the jar file
WORKDIR /app
# Switch to the new user
USER myuser

LABEL maintainer="yadi"
COPY target/assignment-0.0.1-SNAPSHOT.jar ./practice-app.jar

ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","./practice-app.jar"]
